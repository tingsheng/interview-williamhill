const gulp = require('gulp');

gulp.task('moveJquery', () => {
    return gulp
        .src('node_modules/jquery/*/*')
        .pipe(gulp.dest('src/public/libs/jquery'));
});

gulp.task('moveBootstrap', ['moveJquery'], () => {
    return gulp
        .src('node_modules/bootstrap/*/*/*')
        .pipe(gulp.dest('src/public/libs/bootstrap/'));
});

gulp.task('move', ['moveJquery', 'moveBootstrap'], () => {});
