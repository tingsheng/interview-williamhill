require('./config/config');
const express = require('express');
const app = express();
const hbs = require('hbs');
const PORT = process.env.PORT;

const routes = require('./routes');

//public middleware
app.use(express.static(__dirname + '/public'));
//template engine setup: handlebars
app.set('views', '' + __dirname + '/views');
app.set('view engine', 'hbs');
app.engine('hbs', hbs.__express);

app.use('/parser', routes.parser);
app.use('/*', (req, res) => {
    res.send("this route doesn't exist");
});

app.listen(PORT, () =>
    console.log(`server USER now listening on port ${PORT}`)
);
