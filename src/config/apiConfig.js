module.exports = {
    USERS_JSON: 'users.json',
    USERS_XML: 'users.xml',
    USERS_CSV: 'users.csv'
};
