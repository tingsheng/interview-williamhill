const axios = require('axios');

const axiosInstance = axios.create({
    baseURL: `${process.env.API_HOST}api`,
    timout: 10000
});

module.exports = axiosInstance;
