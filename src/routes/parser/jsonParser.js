const {usersJsonApi} = require('./api');

module.exports = async (req, res) => {
    try {
        const {data} = await usersJsonApi();
        const namesStartWithA = data.filter(
            item => item.first_name[0].toUpperCase() === 'A'
        );
        const namesNotStartWithA = data.filter(
            item => !namesStartWithA.includes(item)
        );

        res.setHeader('content-type', 'text/html; charset=UTF-8');
        res.render('user-json.hbs', {
            namesStartWithA,
            namesNotStartWithA
        });
    } catch (err) {
        console.log('err = ', err);
    }
};
