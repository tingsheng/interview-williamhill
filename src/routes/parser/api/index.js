const usersJson = require('./usersJson');
const usersXml = require('./usersXml');
const usersCsv = require('./usersCsv');

module.exports = {
    usersJsonApi: usersJson,
    usersXmlApi: usersXml,
    usersCsvApi: usersCsv
};
