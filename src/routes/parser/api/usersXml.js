const apiConfig = require('../../../config/apiConfig');
const apiClient = require('../../../utilities/apiClient');

module.exports = () => apiClient.get(apiConfig.USERS_XML);
