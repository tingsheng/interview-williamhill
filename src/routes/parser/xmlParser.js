const xml2json = require('xml2json');
const Json2csv = require('json2csv').Parser;

const {usersXmlApi} = require('./api');

const fields = ['first_name', 'last_name', 'email', 'gender', 'ip_address'];
const parser = new Json2csv({fields});
let csvData;

module.exports = {
    xml2csvRoute: async (req, res) => {
        try {
            const {data} = await usersXmlApi();
            const jsonData = JSON.parse(xml2json.toJson(data));
            csvData = parser.parse(jsonData.person.person);
            res.render('user-csv.hbs', {
                csvData
            });
        } catch (err) {
            console.log('err = ', err);
        }
    },
    xml2csvDownload: (req, res) => {
        res.send(csvData);
    }
};
