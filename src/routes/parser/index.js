const express = require('express');
const router = express.Router();

const jsonParser = require('./jsonParser');
const {xml2csvRoute, xml2csvDownload} = require('./xmlParser');

router.get('/json', jsonParser);
router.get('/xml', xml2csvRoute);
router.get('/xml/download', xml2csvDownload);

module.exports = router;
