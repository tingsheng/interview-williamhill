# Table of Contents

1.  [Tech Stack](#tech-stack)
2.  [How to install](#how-to-use)
3.  [How to use](#how-to-use)
4.  [Project Structure](#project-structure)
5.  [What's more](#What's more)

## Tech Stack

1.  node.js
2.  express.js
3.  handlebars - rendering engine
4.  bootstrap

## How to install

`npm install (or yarn)`

`gulp move` - move bootstrap and jquery in public folder

`npm run prod` - run node with pm2

how to stop >
`npm run prod-stop`

## How to use

1.  clone repository `mock-api-gp` first and boot up the server following the instruction
    https://bitbucket.org/wojteks27/mock-api-gp

2.  If not configured differently (by explicitly setting environment variable PORT) the API will be exposed on port `3031`.

server exposes two endpoints:

-   /parser/json
-   /parser/xml

## Project Structure

    src
    ├── config : config
    ├── public : static files
    ├── routes : main project logic
    ├── utilities : utilities
    ├── views : rendering template files
    ├── app.js : entry point

## What's more

If you want ot expend the project more, you can do the following

1.  Change the url in config file to deploy on the real cloud service
2.  Use log4js to help debugging
